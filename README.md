# Owlgalunggung-website
Owlgalunggung front-end website repository 

## Features
* Fully responsive layout
* Font awesome icons
* Cross browser support
* Bootstrap grid, 
* Pretty Slider
* Awesome look
* Smooth transition effects
* Valid HTML5 &amp; CSS
* SEO Friendly Code

## Credits
* JavaScript Framework [jQuery](https://pages.github.com/)
* [Google fonts](https://fonts.google.com/), [Fontawesome](http://fontawesome.io/)
* [Bootstrap CSS Framework](http://getbootstrap.com/)

## License
See LICENSE

## Copyright
&copy; [Owlgalunggung](http://www.owlgalunggung.org)
Hyang Language Foundation, Jakarta - Indonesia
